# MUNE

App to try React with Capacitor and Typescript

## Prerequisite
- node v16.13.2
- npm 8.1.2

## Development
Run:
```
npm i
```
In order to install required node modules.
___
### For Android
Follow Capacitor's official documentation to set your environment. You should, among other things, install Android Studio and set your SDK parameters.

Run:
```
npx cap add android
```
In order to configure capacitor for Android.

At this point, you should be able to run the command ```npm run build``` which create the build and also configure Android build.

Use ```npx cap open android``` to start Android Studio easily from your project folder (Android Studio must be properly configured).

You can test your project with your phone or with an emulator.
___
### For IOS
*Section may come later...*
___

## Running the App
You can use ```npm run start``` to run the App in your browser (currently Firefox does well recognized mobile App). FileSystem does work and files can be found under the **storage** tab in your browser's inspector.

Some features may not work. A non-exhaustive list of features not working on browser but working on Android devices:
- Dialogs titles