import 'components/header/header.scss';
import { Icon, ICON_NAMES } from "components/icons/icon";

export const Header = () => {
  return (
    <div className="header">
      <div className="header__menu">
        <Icon iconName={ICON_NAMES.MENU} iconSize="32" />
      </div>
      <div className="header__title">Mune</div>
    </div>
  )
}