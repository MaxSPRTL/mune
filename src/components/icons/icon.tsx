import React from "react"
import { IconContext } from "react-icons"
import { FiEdit, FiMenu, FiPlusSquare, FiTrash, FiX } from "react-icons/fi"

export enum ICON_NAMES {
  EDIT = "EDIT",
  TRASH = "TRASH",
  MENU = "MENU",
  PLUS = "PLUS",
  CROSS = "CROSS"
}

export interface IPropsIcon {
  iconName: ICON_NAMES,
  iconColor?: string,
  iconSize?: string
}

export const Icon = (props: IPropsIcon) => {
  const getIcon = () => {
    switch (props.iconName) {
      case ICON_NAMES.EDIT:
        return <FiEdit />
      case ICON_NAMES.TRASH:
        return <FiTrash />
      case ICON_NAMES.MENU:
        return <FiMenu />
      case ICON_NAMES.PLUS:
        return <FiPlusSquare />
      case ICON_NAMES.CROSS:
        return <FiX />
      default:
        return null
    }
  }

  const { iconColor, iconSize } = props
  return (
    <IconContext.Provider value={{ color: iconColor, size: iconSize }}>
      {getIcon()}
    </IconContext.Provider>
  )
}