import { Header } from "components/header/header";
import "pages/App.scss";
import { MealsPage } from "pages/meals/mealsPage";

function App() {
  return (
    <div className="app">
      <div className="app__header">
        <Header />
      </div>
      <div className="app__body">
        <MealsPage />
      </div>
    </div>
  );
}

export default App;
