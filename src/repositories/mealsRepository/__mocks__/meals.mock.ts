import { IMeal } from "types/meals/IMeal";

export const mealsMocked: IMeal[] = [
  {
    id: "1",
    name: "Tagliatelles au saumon fumé",
    createdAt: 1643121551797,
    updatedAt: null
  },
  {
    id: "2",
    name: "Blablabla truc méga long pour faire un test d'affichage",
    createdAt: 1643121551797,
    updatedAt: null
  },
  {
    id: "3",
    name: "Soupe",
    createdAt: 1643187684130,
    updatedAt: null
  }
]