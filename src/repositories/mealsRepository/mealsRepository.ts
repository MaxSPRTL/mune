import { FILES_PATH } from "constants/files.constants";
import { FileSystemService } from "services/fileSystem.service";
import { IMeal } from "types/meals/IMeal";
import { v4 as uuidv4 } from 'uuid';

export namespace MealsRepository {
  export const get = async () => {
    const doesFileExist = await FileSystemService.doesFileExist({ path: FILES_PATH.MEALS })
    if (doesFileExist) {
      const fileResult = await FileSystemService.readFromFile({ path: FILES_PATH.MEALS })
      const meals: IMeal[] = JSON.parse(fileResult.data)
      return meals
    } else {
      return []
    }
  }

  export const create = ({ name }: { name: string }) => {
    const meal: IMeal = {
      id: uuidv4(),
      name: name,
      createdAt: Date.now(),
      updatedAt: null
    }
    return { ...meal }
  }

  export const save = async (meals: IMeal[]) => {
    const data = JSON.stringify(meals)
    await FileSystemService.writeToFile({ path: FILES_PATH.MEALS, data })
  }
}