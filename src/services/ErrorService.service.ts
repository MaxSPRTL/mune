import i18next from "i18next"
import { IError } from "types/errors/IError"
import { DialogService } from "./dialog.service"

export namespace ErrorService {
  export const handle = async (error: IError) => {
    const unspecifiedError = i18next.t('errors.whoops')
    const errorMessage = error.userMessage || unspecifiedError
    await DialogService.showAlert({
      title: unspecifiedError,
      message: errorMessage
    })
    console.error(error.technicalError)
  }
}