import { Directory, Encoding, Filesystem } from '@capacitor/filesystem';

export namespace FileSystemService {
  const APP_FILESYSTEM_PARAMS = {
    directory: Directory.Data,
    encoding: Encoding.UTF8
  }

  export const writeToFile = async (params: { path: string, data: string }) => {
    return await Filesystem.writeFile({
      ...params,
      ...APP_FILESYSTEM_PARAMS,
      recursive: true
    })
  }

  export const readFromFile = async (params: { path: string }) => {
    return await Filesystem.readFile({
      ...params,
      ...APP_FILESYSTEM_PARAMS
    })
  }

  export const getStatsOfFile = async (params: { path: string }) => {
    return await Filesystem.stat({
      ...params,
      ...APP_FILESYSTEM_PARAMS
    })
  }

  export const doesFileExist = async (params: { path: string }) => {
    try {
      await FileSystemService.getStatsOfFile({ ...params })
      return true
    } catch (err) {
      return false
    }
  }
}
