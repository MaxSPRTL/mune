import i18next from "i18next"
import { MealsRepository } from "repositories/mealsRepository/mealsRepository"
import { ErrorService } from "services/ErrorService.service"
import { ToastService } from "services/toast.service"
import { AppDispatch, RootState } from "store/store"
import { mealsSlice } from "./mealsSlice"

export namespace MealsActions {
  export const fetchMeals = () => async (dispatch: AppDispatch) => {
    try {
      const meals = await MealsRepository.get()
      dispatch(mealsSlice.actions.setMeals(meals))
      dispatch(mealsSlice.actions.sortMeals())
    } catch (err) {
      ErrorService.handle({
        technicalError: err,
        userMessage: i18next.t('meals.errors.onFetch')
      })
      dispatch(mealsSlice.actions.setMeals([]))
    }
  }

  export const createMeal = (name: string) => (dispatch: AppDispatch) => {
    if (name) {
      const addedMeal = MealsRepository.create({ name })
      dispatch(mealsSlice.actions.addMeal(addedMeal))
      dispatch(mealsSlice.actions.sortMeals())
      ToastService.displayToast({
        text: i18next.t('meals.actions.created')
      })
    }
  }

  export const deleteMeal = (mealId: string) => (dispatch: AppDispatch) => {
    dispatch(mealsSlice.actions.removeMeal(mealId))
    ToastService.displayToast({
      text: i18next.t('meals.actions.deleted')
    })
  }

  export const saveMeals = () => async (dispatch: AppDispatch, getState: () => RootState) => {
    try {
      const meals = getState().mealsReducer.meals
      await MealsRepository.save(meals)
    } catch (err) {
      ErrorService.handle({
        technicalError: err,
        userMessage: i18next.t('meals.errors.onSave')
      })
    }
  }
}