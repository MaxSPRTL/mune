import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { IMeal } from 'types/meals/IMeal'

interface IMealsState {
  meals: IMeal[]
}

const initialState: IMealsState = {
  meals: []
}

export const mealsSlice = createSlice({
  name: 'meals',
  initialState,
  reducers: {
    setMeals: (state, action: PayloadAction<IMeal[]>) => {
      state.meals = [...action.payload]
    },

    addMeal: (state, action: PayloadAction<IMeal>) => {
      const meal = action.payload
      state.meals = [...state.meals, { ...meal }]
    },

    removeMeal: (state, action: PayloadAction<string>) => {
      const mealId = action.payload
      state.meals = [...state.meals.filter(meal => meal.id !== mealId)]
    },

    sortMeals: (state) => {
      state.meals = [...state.meals.sort((a, b) => a.name > b.name ? 1 : -1)]
    }
  },
})

export const mealsReducer = mealsSlice.reducer