export interface IError {
  technicalError: any,
  userMessage?: string
}