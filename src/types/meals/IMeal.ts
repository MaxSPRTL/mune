export interface IMeal {
  id: string,
  name: string,
  createdAt: number,
  updatedAt: number | null
}